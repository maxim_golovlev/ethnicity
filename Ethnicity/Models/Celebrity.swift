//
//  Celebrity.swift
//  Ethnicity
//
//  Created by  Macbook on 02.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import Foundation

protocol PropertyReflectable { }

extension PropertyReflectable {
    subscript(key: String) -> Any? {
        let m = Mirror(reflecting: self)
        for child in m.children {
            if child.label == key { return child.value }
        }
        return nil
    }
}

class NetworthCeleb: Decodable, PropertyReflectable {
    var name: String!
    var birthName: String?
    var birthPlace: String?
    var picture: String?
    var celebYears: CelebYears?
    var description: String?
    var netWorth: NetWorth?
    var similar: [String]?
    var similar2: [String]?
    var height: CelebHeight?
    var salary: String?
    var gender: Gender?
    var nationality: String?
    var profession: [String]?
    var ethnicity: [String]?
    var tags: [String]?
    var categories: [String]?
    
    enum CodingKeys: String, CodingKey {
        case name
        case birthName
        case birthPlace
        case picture = "image-src"
        case celebYears = "dateOfBirth"
        case description
        case netWorth
        case similar = "more"
        case similar2
        case height
        case salary
        case gender
        case nationality
        case profession
        case ethnicity
        case tags
        case categories
    }
    
    init(){

    }
    
    required init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            name = (try? container.decode(String?.self, forKey: .name)) ?? ""
            birthName = try? container.decode(String?.self, forKey: .birthName)
            birthPlace = try? container.decode(String?.self, forKey: .birthPlace)
            picture = try? container.decode(String?.self, forKey: .picture)
            if let celebYears = try? container.decode(CelebYears?.self, forKey: .celebYears) {
                self.celebYears = celebYears
            } else if let celebYears = try? container.decode(String?.self, forKey: .celebYears) {
                self.celebYears = CelebYears(string: celebYears)
            }
            description = try? container.decode(String?.self, forKey: .description)
            netWorth = try? container.decode(NetWorth?.self, forKey: .netWorth)
            similar = try? container.decode([String]?.self, forKey: .similar)
            similar2 = try? container.decode([String]?.self, forKey: .similar2)
            height = try? container.decode(CelebHeight?.self, forKey: .height)
            salary = try? container.decode(String?.self, forKey: .salary)
            gender = try? container.decode(Gender?.self, forKey: .gender)
            nationality = try? container.decode(String?.self, forKey: .nationality)
            profession = try? container.decode([String]?.self, forKey: .profession)
            ethnicity = try? container.decode([String]?.self, forKey: .ethnicity)
            tags = try? container.decode([String]?.self, forKey: .tags)
        } catch {
            if case .keyNotFound? = error as? DecodingError {
                return
            }
        }
    }
}

struct CelebYears: Codable {
    var dateOfBirth: Date?
    var dateOfDeath: Date?
    
    init(dateOfBirth: Date?, dateOfDeath: Date?) {
        self.dateOfBirth = dateOfBirth
        self.dateOfDeath = dateOfDeath
    }
    
    init?(string: String?) {
        
        guard var dateOfBirthRaw = string else { return nil }
        
        if dateOfBirthRaw.count > 28 {
            if let startIndex = dateOfBirthRaw.firstIndex(of: "("),
                let endIndex = dateOfBirthRaw.firstIndex(of: ")") {
            
                dateOfBirthRaw.replaceSubrange(startIndex...endIndex, with: "")
                let array = dateOfBirthRaw.components(separatedBy: " - ").compactMap({ (string: String) -> (Date?) in
                    
                    let cleanString = string.trimmingCharacters(in: .whitespacesAndNewlines)
                    
                    if let date = cleanString.toDate(format: "MMM d, yyyy") {
                        return date
                    } else if let date = cleanString.toDate(format: "MMM, yyyy") {
                        return date
                    }
                    return nil
                })
                dateOfBirth = array[0]
                dateOfDeath = array[1]
            }
            
        } else if dateOfBirthRaw.contains("-") == true {
            dateOfBirth = dateOfBirthRaw.trimmingCharacters(in: .whitespacesAndNewlines).toDate(format: "yyyy-MM-dd")
        } else {
            
            if let startIndex = dateOfBirthRaw.firstIndex(of: "("),
                let endIndex = dateOfBirthRaw.firstIndex(of: ")") {
            
                dateOfBirthRaw.replaceSubrange(startIndex...endIndex, with: "")
                dateOfBirth = dateOfBirthRaw.trimmingCharacters(in: .whitespacesAndNewlines).toDate(format: "MMM d, yyyy")
            }
        }
    }
    
    var age: Int? {
        
        if let dateOfBirth = dateOfBirth, let dateOfDeath = dateOfDeath {
            let years = Calendar.current.dateComponents([.year], from: dateOfBirth, to: dateOfDeath).year!
            return years
        }
        
        if let dateOfBirth = dateOfBirth, dateOfDeath == nil {
            let years = Calendar.current.dateComponents([.year], from: dateOfBirth, to: Date()).year!
            return years
        }
        
        return nil
    }
    
    var string: String? {
        
        if let dateOfBirth = dateOfBirth, let dateOfDeath = dateOfDeath {
            let years = Calendar.current.dateComponents([.year], from: dateOfBirth, to: dateOfDeath).year!
            return dateOfBirth.toString(format: "MMM d, yyyy") + " - " + dateOfDeath.toString(format: "MMM d, yyyy") + " (\(years) years old)"
        }
        
        if let dateOfBirth = dateOfBirth, dateOfDeath == nil {
            let years = Calendar.current.dateComponents([.year], from: dateOfBirth, to: Date()).year!
            return dateOfBirth.toString(format: "MMM d, yyyy") + " (\(years) years old)"
        }

        return nil
    }
}

extension NetworthCeleb: Encodable {
    
    func encode(to encoder: Encoder) throws {
        do {
            
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(name, forKey: .name)
            try container.encode(birthName, forKey: .birthName)
            try container.encode(birthPlace, forKey: .birthPlace)
            try container.encode(picture, forKey: .picture)
            try container.encode(celebYears, forKey: .celebYears)
            try container.encode(description, forKey: .description)
            try container.encode(netWorth, forKey: .netWorth)
            try container.encode(similar, forKey: .similar)
            try container.encode(similar2, forKey: .similar2)
            try container.encode(height, forKey: .height)
            try container.encode(salary, forKey: .salary)
            try container.encode(gender, forKey: .gender)
            try container.encode(nationality, forKey: .nationality)
            try container.encode(profession, forKey: .profession)
            try container.encode(ethnicity, forKey: .ethnicity)
            try container.encode(tags, forKey: .tags)
            try container.encode(categories, forKey: .categories)
        } catch {
            if case .keyNotFound? = error as? DecodingError {
                return
            }
        }
    }
}

enum Gender: String, Codable {
    case male = "Male"
    case female = "Female"
}

class Celebrity: Decodable {
    
    var name: String?
    var picture: String?
    var birthName: String?
    var birthPlace: String?
    var deathPlace: String?
    var dateOfBirth: String?
    var dateOfDeath: String?
    var description: String?
    var ethnicity: [String]?
    var sources: [String]?
    var similar: [String]?
    var tags: [String]?
    var categories: [String]?
    var height: Height?
    var netWorth: NetWorth?
    
    enum CodingKeys: String, CodingKey {
        case name
        case picture
        case birthName
        case birthPlace
        case deathPlace
        case dateOfBirth
        case dateOfDeath
        case description
        case ethnicity
        case rawEthnicity
        case sources
        case similar
        case tags
        case categories
        case height
        case netWorth
    }
    
    required init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            name = try? container.decode(String?.self, forKey: .name)
            picture = try? container.decode(String?.self, forKey: .picture)
            birthName = try? container.decode(String?.self, forKey: .birthName)
            birthPlace = try? container.decode(String?.self, forKey: .birthPlace)
            deathPlace = try? container.decode(String?.self, forKey: .deathPlace)
            dateOfBirth = try? container.decode(String?.self, forKey: .dateOfBirth)
            dateOfDeath = try? container.decode(String?.self, forKey: .dateOfDeath)
            description = try? container.decode(String?.self, forKey: .description)
            ethnicity = try? container.decode([String]?.self, forKey: .ethnicity)
            similar = try? container.decode([String]?.self, forKey: .similar)
            tags = try? container.decode([String]?.self, forKey: .tags)
            categories = try? container.decode([String]?.self, forKey: .categories)
            height = try? container.decode(Height?.self, forKey: .height)
            netWorth = try? container.decode(NetWorth?.self, forKey: .netWorth)
        } catch {
            if case .keyNotFound? = error as? DecodingError {
                return
            }
        }
    }
    
}

extension Celebrity: Encodable {
    
    func encode(to encoder: Encoder) throws {
        do {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(name, forKey: .name)
            try container.encode(picture, forKey: .picture)
            try container.encode(birthName, forKey: .birthName)
            try container.encode(birthPlace, forKey: .birthPlace)
            try container.encode(deathPlace, forKey: .deathPlace)
            try container.encode(dateOfBirth, forKey: .dateOfBirth)
            try container.encode(dateOfDeath, forKey: .dateOfDeath)
            try container.encode(description, forKey: .description)
            try container.encode(ethnicity, forKey: .ethnicity)
            try container.encode(similar, forKey: .similar)
            try container.encode(tags, forKey: .tags)
            try container.encode(categories, forKey: .categories)
            try container.encode(height, forKey: .height)
            try container.encode(netWorth, forKey: .netWorth)
        } catch {
            if case .keyNotFound? = error as? DecodingError {
                return
            }
        }
    }
}

class Ethnicity:  Codable {
    
    var name: String?
    var percent: String?
    
    init(name: String?, percent: String?) {
        self.name = name
        self.percent = percent
    }
}

class Comment: Codable {
    
    var text: String?
    var date: Date?
    var user: User?
    var replies: [Comment]?
}

class Height: Codable {
    
    var name: String?
    var height: String?
    var value: Int = 0
    
    init(name: String?, height: String?, value: Int) {
        self.name = name
        self.height = height
        self.value = value
    }
}

class User: Codable {
    
    var name: String?
    var avatar: String?
}

class NetWorth: Codable {
    
    enum Names: String {
        case thousand
        case million
        case billion
        case thrillion
        
        var value: Double {
            switch self {
            case .thousand:
                return 1000
            case .million:
                return 1000_000
            case .billion:
                return 1000_000_000
            case .thrillion:
                return 1000_000_000_000
            }
        }
    }
    
    var string: String?
    
    var value: Double? {
        
        let s = string?.replacingOccurrences(of: "$", with: "")
        let a = s?.components(separatedBy: " ")
        
        if let number = a?[safe: 0],
            let double = Double(number),
            let string = a?[safe: 1]?.lowercased(),
            let nameValue = Names(rawValue: string)?.value {
            
            return double * nameValue
        }
        
        return nil
    }
    
    init(value: String?) {
        self.string = value
    }
}

class CelebHeight: Codable, Comparable {
    
    private var feetValue: Measurement<UnitLength>?
    private var inchValue: Measurement<UnitLength>?
    
    private var feetValueDouble: Double?
    private var inchValueDouble: Double?
    
    var cmValueDouble: Double? {
        var result: Double = 0
        
        if let feetValue = feetValueDouble {
            result +=  feetValue * 12 * 2.54
        }
        
        if let inchValue = inchValueDouble {
            result += inchValue * 2.54
        }
        
        result = ceil(result)
        
        return result == 0 ? nil : result
    }
    
    var imperialString: String? {
        
        var result = [String]()
        
        let formatter = MeasurementFormatter()
        formatter.unitOptions = .providedUnit
        formatter.unitStyle = .medium
        
        if let feetValue = feetValue {
            result.append(formatter.string(from: feetValue))
        }
        
        if let inchValue = inchValue {
            result.append(formatter.string(from: inchValue))
        }
        return result.compactMap({ $0 }).joined(separator: " ")
    }
    
    var metricalString: String? {
        guard let cmValueDouble = cmValueDouble else { return nil }
        return String(format: "%.0f cm", cmValueDouble)
    }
    
    var fullstring: String? {
        
        var result: String = ""
        
        if let imperialString = imperialString {
            result += imperialString
        }
        
        if let metricalString = metricalString {
            result += " (\(metricalString))"
        }
        
        return result.isEmpty ? nil : result
    }
    
    init(string: String) {
        
        var sourceString = string
        
        if let topBound = sourceString.firstIndex(of: "("), let botBound = sourceString.firstIndex(of: ")") {
            sourceString.removeSubrange(topBound...botBound)
            sourceString = sourceString.trimmingCharacters(in: .whitespaces)
            
            let a = sourceString.components(separatedBy: CharacterSet.decimalDigits.inverted).filter({ !$0.isEmpty }).compactMap({ Double($0) })
            feetValueDouble = a[safe: 0]
            inchValueDouble = a[safe: 1]
            
            if let feet = a[safe: 0] {
                feetValue = Measurement(value: feet, unit: UnitLength.feet)
            }
            
            if let inch = a[safe: 1] {
                inchValue = Measurement(value: inch, unit: UnitLength.inches)
            }
        }
        
    }
    
    static func < (lhs: CelebHeight, rhs: CelebHeight) -> Bool {
        guard let lhs = lhs.cmValueDouble, let rhs = rhs.cmValueDouble else {
            return false
        }
        return lhs < rhs
    }
    
    static func == (lhs: CelebHeight, rhs: CelebHeight) -> Bool {
        guard let lhs = lhs.cmValueDouble, let rhs = rhs.cmValueDouble else {
            return false
        }
        return lhs == rhs
    }
    
}


