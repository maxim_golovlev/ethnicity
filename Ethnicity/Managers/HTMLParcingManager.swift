//
//  HTMLParcingManager.swift
//  CelebEthnicity
//
//  Created by User on 18.10.2018.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import SwiftSoup


enum ErrorType: Error {
    case netwothError
}

class HTMLParcingManager {
    
    enum CelebtiryTags: String {
        case birthName = "Birth Name:"
        case birthPlace = "Place of Birth:"
        case dateOfBirth = "Date of Birth:"
        case dateOfDeath = "Date of Death:"
        case placeOfDeath = "Place of Death:"
        case ethnicity = "Ethnicity:"
    }
    
    typealias Item = (text: String, html: String)
    private (set) var document: Document = Document.init("")
    
    var originalHTML: String!
    var imgHTML: String!
    var heights = [Height]()

    @discardableResult
    func downloadHTML(urlString: String?) -> String? {
        // url string to URL
        guard let url = URL(string: urlString ?? "") else {
            // an error occurred
            print("Error: \(urlString ?? "") doesn't seem to be a valid URL")
            return nil
        }
        
        do {
            // content of url
            let html = try String.init(contentsOf: url)
            // parse it into a Document
            document = try SwiftSoup.parse(html)
            return html
        } catch let error {
            // an error occurred
            print("Error: \(error)")
            return nil
        }
    }
    
    func changeHtml(_ html: String?) {
        guard let html = html else { return }
        
        do {
            document = try SwiftSoup.parse(html)
        } catch let error {
            print("Error: \(error)")
        }
    }
    
    //Parse CSS selector
    func parse(cssQuery: String?) -> [Item]? {
        do {
            var items = [Item]()
            // firn css selector
            let elements: Elements = try document.select(cssQuery ?? "")
            //transform it into a local object (Item)
            for element in elements {
                let text = try element.text()
                let html = try element.outerHtml()
                items.append(Item(text: text, html: html))
            }
            return items
            
        } catch let error {
            print("Error: \(error)")
        }
        
        return nil
    }
}

extension HTMLParcingManager {
    
    func collectHeights(letter: String, completion: ([Height])->()) {
        
        var components = URLComponents()
        components.scheme = "http"
        components.host = "celebheights.com"
        components.path = "/s/all\(letter).html"
        
        downloadHTML(urlString: components.string)
        var titles = parse(cssQuery: ".sAZ2 a")!.compactMap({ $0.text }).filter({ $0 != "" })
        var heights = parse(cssQuery: ".sAZ2")!.compactMap({ $0.text }).filter({ $0 != "" })
        
        heights.enumerated().forEach({ (string) in
            heights[string.offset] = string.element.replacingOccurrences(of: titles[string.offset], with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        })

        let cmValues = heights.map({ $0.slice(from: "(", to: ")")?.replacingOccurrences(of: "cm", with: "") }).map({ $0 ?? "0" }).compactMap({ Int($0) })
        
        let heightsClass = heights.enumerated().compactMap({ Height(name: titles[$0.offset], height: $0.element, value: cmValues[$0.offset]) })
        
        completion(heightsClass)
    }
    
    func collectdataForCeleb(celeb: Celebrity, url: URL? = nil, completion: @escaping (Celebrity?)->()) {
    
        let celebName = celeb.name?.components(separatedBy: " ").joined(separator: "-")
        .replacingOccurrences(of: "’", with: "")
        .replacingOccurrences(of: "ă", with: "a")
        .replacingOccurrences(of: "é", with: "e")
        .replacingOccurrences(of: "ó", with: "o")
        .replacingOccurrences(of: "í", with: "i")
        .replacingOccurrences(of: "(", with: "")
        .replacingOccurrences(of: ")", with: "")
        .replacingOccurrences(of: "-城田優", with: "")
        .replacingOccurrences(of: "á", with: "a")
        .replacingOccurrences(of: "ö", with: "o")
        .replacingOccurrences(of: "ú", with: "u")
        .replacingOccurrences(of: "“", with: "")
        .replacingOccurrences(of: "š", with: "s")
        .replacingOccurrences(of: "Š", with: "S")
        .replacingOccurrences(of: "ř", with: "r")
        .replacingOccurrences(of: "ñ", with: "n")
        .replacingOccurrences(of: "Ó", with: "O")
        .replacingOccurrences(of: "ø", with: "o")
        .replacingOccurrences(of: "Ö", with: "o")
        
        if let url = url {
            originalHTML = downloadHTML(urlString: url.absoluteString)
            
        } else {
            var components = URLComponents()
            components.scheme = "http"
            components.host = "ethnicelebs.com"
            components.path = "/\(celebName ?? "")"
            originalHTML = downloadHTML(urlString: components.string)
        }
        
        let items = parse(cssQuery: "p")
        
        let categories = (parse(cssQuery: ".clean-grid-entry-meta-single-cats")?.first?.text ?? "").replacingOccurrences(of: "Posted in", with: "").trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: ", ").flatMap({ $0.components(separatedBy: " ").dropFirst() })
        
        let name = parse(cssQuery: ".entry-title")?.first?.text ?? ""
        let birthNameRaw = items?.first(where: { $0.text.contains(CelebtiryTags.birthName.rawValue) })?.text
        let birthName = birthNameRaw?.replacingOccurrences(of: CelebtiryTags.birthName.rawValue, with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        let birthPlaceRaw = items?.first(where: { $0.text.contains(CelebtiryTags.birthPlace.rawValue) })?.text
        let birthPlace = birthPlaceRaw?.replacingOccurrences(of: CelebtiryTags.birthPlace.rawValue, with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        let dateOfBirthRaw = items?.first(where: { $0.text.contains(CelebtiryTags.dateOfBirth.rawValue) })?.text
        let dateOfBirth = dateOfBirthRaw?.replacingOccurrences(of: CelebtiryTags.dateOfBirth.rawValue, with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        let deathPlaceRaw = items?.first(where: { $0.text.contains(CelebtiryTags.placeOfDeath.rawValue) })?.text
        let deathPlace = deathPlaceRaw?.replacingOccurrences(of: CelebtiryTags.placeOfDeath.rawValue, with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        let dateOfDeathRaw = items?.first(where: { $0.text.contains(CelebtiryTags.dateOfDeath.rawValue) })?.text
        let dateOfDeath = dateOfDeathRaw?.replacingOccurrences(of: CelebtiryTags.dateOfDeath.rawValue, with: "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        let ethnicityRaw = items?.first(where: { $0.text.contains(CelebtiryTags.ethnicity.rawValue) })?.text
        let ethnicityString = ethnicityRaw?.replacingOccurrences(of: CelebtiryTags.ethnicity.rawValue, with: "")
        let ethnicity = ethnicityString?.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: ", ")
        let ethnicityEntities = ethnicity?.compactMap({ Ethnicity(name: $0, percent: nil) })
        let tags = parse(cssQuery: "a[href*=tag]")?.compactMap({ $0.text })
        
       // let similarHTML = parse(cssQuery: ".yarpp-related")?.first?.html
      //  changeHtml(similarHTML)
     //   let similar = parse(cssQuery: "a[href]")?.compactMap({ $0.text }) ?? []
        
//        let content = [birthNameRaw, birthPlaceRaw, dateOfBirthRaw, deathPlaceRaw, dateOfDeathRaw, ethnicityRaw]
//
//        let descriptionRaw = parse(cssQuery: ".entry-inner p")?.compactMap({ $0.text }) ?? []
//
//        let description = descriptionRaw.filter({ !content.contains($0) }).filter({ !$0.contains("photo by") }).filter({ !$0.contains("Photo by") }).filter({ !$0.isEmpty }).filter({ !$0.contains("Shutterstock") }).filter({ !$0.contains("Photo Credit") })
//
//        celeb.description = description.joined(separator: "\n\n")
        if let birthName = birthName {
            celeb.birthName = birthName
        }
        if let birthPlace = birthPlace {
            celeb.birthPlace = birthPlace
        }
        if let dateOfBirth = dateOfBirth {
            celeb.dateOfBirth = dateOfBirth
        }
        if let dateOfDeath = dateOfDeath {
            celeb.dateOfDeath = dateOfDeath
        }
        if let deathPlace = deathPlace {
            celeb.deathPlace = deathPlace
        }
        if let ethnicity = ethnicity {
            celeb.ethnicity = ethnicity
        }
        if let tags = tags {
            celeb.tags = tags
        }
        
        completion(celeb)
        
    }
    
}
