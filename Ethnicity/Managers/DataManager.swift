//
//  DataManager.swift
//  Ethnicity
//
//  Created by  Macbook on 02.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import Foundation

class DataManager {
    
    static var cashedCelebs: [NetworthCeleb]?

    static func fetchHeights() -> [Height]{
        
        if let path = Bundle.main.path(forResource: "Height", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let items = try! JSONDecoder().decode([Height].self, from: jsonData)
                return items
              } catch {
                   print("cannot load files from json")
              }
        }
        
        return []
    }
    
    static func fetchCelebs() -> [Celebrity]{
        
        if let path = Bundle.main.path(forResource: "CelebrityOld", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let items = try! JSONDecoder().decode([Celebrity].self, from: jsonData)
                return items
              } catch {
                   print("cannot load files from json")
              }
        }
        
        return []
    }
    
    static func fetchNetworthCelebs(completion: @escaping ([NetworthCeleb]?) -> ()){
        
        if let cashedCelebs = cashedCelebs {
            completion(cashedCelebs)
        }
        
        DispatchQueue.global(qos: .background).async {
            
            if let path = Bundle.main.path(forResource: "Celebrity", ofType: "json") {
                do {
                    let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let items = try! JSONDecoder().decode([NetworthCeleb].self, from: jsonData)
                    cashedCelebs = items
                    
                    DispatchQueue.main.async {
                        completion(cashedCelebs)
                        return
                    }
                    
                  } catch {
                       print("cannot load files from json")
                  }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                    return
                }
            }
        }
    }
    
    static func saveCelebrities(celebrities: [Celebrity]) {
        let jsonData = try! JSONEncoder().encode(celebrities)

        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Celebrity.json")
        
        try! jsonData.write(to: fileUrl, options: [])
    }
    
    static func saveNetworthCelebrities(celebrities: [NetworthCeleb]) {
        let jsonData = try! JSONEncoder().encode(celebrities)

        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Celebrity.json")
        
        try! jsonData.write(to: fileUrl, options: [])
    }
    
    static func saveHeights(heights: [Height]) {
        let jsonData = try! JSONEncoder().encode(heights)

        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("Height.json")
        
        try! jsonData.write(to: fileUrl, options: [])
    }
    
    static func updateHeightsAndSave() {
        
        updateHeights { (heights) in
            saveHeights(heights: heights)
        }
        
    }

    static func updateHeights(completion: ([Height]) -> ()) {
            
        let startChar = Unicode.Scalar("A").value
        let endChar = Unicode.Scalar("Z").value
    
        var heights = [Height]()

        for alphabet in startChar...endChar {

            if let char = Unicode.Scalar(alphabet)?.description {
                HTMLParcingManager().collectHeights(letter: char) { (array) in
                    print(char)
                    heights.append(contentsOf: array)
                    if alphabet == endChar {
                        completion(heights)
                    }
                }
            }
        }
    }
    
    static func fetchAthlets() -> [NetworthCeleb]{
        
        if let path = Bundle.main.path(forResource: "richest-athletes", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let items = try! JSONDecoder().decode([NetworthCeleb].self, from: jsonData)
                return items
              } catch {
                   print("cannot load files from json")
              }
        }
        
        return []
    }
    
    static func fetchBusinessmen() -> [NetworthCeleb]{
        
        if let path = Bundle.main.path(forResource: "richest-businessmen", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let items = try! JSONDecoder().decode([NetworthCeleb].self, from: jsonData)
                return items
              } catch {
                   print("cannot load files from json")
              }
        }
        
        return []
    }
    
    static func fetchCelebrities() -> [NetworthCeleb]{
        
        if let path = Bundle.main.path(forResource: "richest-celebrities", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let items = try! JSONDecoder().decode([NetworthCeleb].self, from: jsonData)
                return items
              } catch {
                   print("cannot load files from json")
              }
        }
        
        return []
    }
    
    static func fetchPoliticans() -> [NetworthCeleb]{
        
        if let path = Bundle.main.path(forResource: "richest-politicians", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let items = try! JSONDecoder().decode([NetworthCeleb].self, from: jsonData)
                return items
              } catch {
                   print("cannot load files from json")
              }
        }
        
        return []
    }
}
