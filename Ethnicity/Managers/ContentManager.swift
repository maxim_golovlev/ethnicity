//
//  ContentManager.swift
//  CelebEthnicity
//
//  Created by User on 07.11.2018.
//  Copyright © 2018 User. All rights reserved.
//

import Foundation

class ContentManager {
    
    enum ImageSource {
        case bing
        case wiki
    }
    
    static func setImage(celeb: Celebrity, source: ImageSource = .bing, completion: @escaping (Celebrity?, Error?)->()) {
        
        switch source {
        case .bing:
            getImageUsingBing(query: celeb.name!) { (urlString, error) in
                celeb.picture = urlString
                completion(celeb, error)
            }
            
        case .wiki:
            getImageFromWiki(query: celeb.name!) { (urlString, error) in
                celeb.picture = urlString
                completion(celeb, error)
            }
        }
    }
    
    static func setDescription(celeb: Celebrity, completion: @escaping (Celebrity?, Error?)->()) {
        
        getDescription(name: celeb.name!) { (string, error) in
            celeb.description = string
            completion(celeb, error)
        }
    }
    
    static func getImageUsingBing(query: String, completion: @escaping (String?, Error?)->()) {
        
        let string = "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q=\(query)&count=1&subscription-key=fdb57dedbfd54937a609cd69297ba109&minHeight=500&maxHeight=1000&imageType=photo&maxWidth=500".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        let task = URLSession.shared.dataTask(with: URL(string: string!)!) { (data, response, error) in
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                if let values = json?["value"] as? [[String: Any]] {
                    if let value = values.first {
                        if let source = value["contentUrl"] as? String {
                            completion(source, nil)
                        } else {
                   //         print("no photo")
                            completion(nil, error)
                        }
                    } else {
                 //       print("no photo")
                        completion(nil, error)
                    }
                } else {
               //    print("no photo")
                    completion(nil, error)
                }
            } catch {
             //   print(error)
                completion(nil, error)
            }
        }
        
        task.resume()
    }
    
    static func getImageFromWiki(query: String, completion: @escaping (String?, Error?)->()) {
        
        let string = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=pageimages&redirects=1&titles=\(query)&pithumbsize=500".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        let task = URLSession.shared.dataTask(with: URL(string: string!)!) { (data, response, error) in
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                if let query = json?["query"] as? [String: Any] {
                    if let pages = query["pages"] as? [String: Any] {
                        if let pageId = pages[pages.keys.first!] as? [String: Any] {
                            if let thumbnail = pageId["thumbnail"] as? [String: Any] {
                                if let source = thumbnail["source"] as? String {
                                    
                                    completion(source, nil)
                                }
                            } else {
                               // print("no photo")
                                completion(nil, error)
                            }
                        }
                    }
                }
            } catch {
                print(error)
                completion(nil, error)
            }
        }
        
        task.resume()
    }
    
    static func getDescription(name: String, completion: @escaping (String?, Error?)->()) {
        let string = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=\(name)".addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        
        let url = URL(string: string!)!
        
        let headers = ["Content-Type": "text/html; charset=utf-8"]
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
         //   print("response", response)
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? [String: Any]
                if let query = json?["query"] as? [String: Any] {
                    if let pages = query["pages"] as? [String: Any] {
                        if let pageId = pages[pages.keys.first!] as? [String: Any] {
                            if let extract = pageId["extract"] as? String {
                                
                                completion(extract, nil)
                            } else {
                                print("\(name) no extract")
                                completion(nil, error)
                            }
                        } else {
                            print("\(name) no pages.keys")
                        }
                    } else {
                        print("\(name) no pages")
                    }
                } else {
                    print("\(name) no query")
                }
            } catch {
                print(error)
                completion(nil, error)
            }
        }
        
        task.resume()
    }
    
//    func parseNetWorth(celeb: Celebrity, completion: @escaping (NetWorth?) -> ()) {
//        
//        guard let celebName = celeb.name else {
//            completion(nil)
//            return
//        }
//        
//        var components = URLComponents()
//        components.scheme = "https"
//        components.host = "www.googleapis.com"
//        components.path = "/customsearch/v1"
//        components.queryItems = [
//            URLQueryItem(name: "key", value: "AIzaSyBeMQnnq59dspIfIPJ4OzFPTBmUvlzC-b8"),
//            URLQueryItem(name: "cx", value: "016341778893280683759:svhhdkw6raw"),
//            URLQueryItem(name: "q", value: celebName + " net worth")
//        ]
//        
//        let url = components.url!
//        
//        let headers = ["Content-Type": "application/json; charset=UTF-8"]
//        var request = URLRequest(url: url)
//        request.httpMethod = "GET"
//        request.allHTTPHeaderFields = headers
//        
//        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
//            do {
//                let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? [String: Any]
//                if let items = json?["items"] as? [[String: Any]] {
//                    
//                    if let link = items.first?["link"] as? String {
//                        
//                        HTMLParcingManager().parseNetWorth(string: link) { (netWorth) in
//                            completion(netWorth)
//                        }
//                        
//                    } else {
//                        print("\(celeb.name!) no data")
//                    }
//                    
//                } else {
//                    print("\(celeb.name!) no items")
//                }
//            } catch {
//                print(error)
//                completion(nil)
//            }
//        }
//        
//        task.resume()
//
//    }
    
}
