//
//  TagsFlowLayout.swift
//  BackMyCash
//
//  Created by User on 15.05.2019.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class TagsFlowLayout: UICollectionViewFlowLayout {
    
    var collectionSize: CGSize = CGSize(width: 1000, height: 1000)
    
    override var collectionViewContentSize: CGSize {
        let oldSize = super.collectionViewContentSize
        let newSize = CGSize(width: oldSize.width, height: collectionSize.height)
        print("newsize ", oldSize.width, newSize.height)
        return newSize
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        let attributesForElementsInRect = super.layoutAttributesForElements(in: rect)
        
        var newAttributesForElementsInRect = [UICollectionViewLayoutAttributes]()
        
        // use a value to keep track of left margin
        var leftMargin: CGFloat = 0.0
        var topMargin: CGFloat = 0.0
        var cellHeight: CGFloat = 0.0
        
        attributesForElementsInRect?.enumerated().forEach({
            
            let attributes = $1
            var newLeftAlignedFrame = attributes.frame
            
            cellHeight = attributes.frame.size.height
            
            if attributes.bounds.width + leftMargin > rect.width, $0 != 0 {
                // assign value if next row
                topMargin += attributes.frame.size.height
                leftMargin = self.sectionInset.left
            }
            
            // set x y position of attributes to current margin
            newLeftAlignedFrame.origin.x = leftMargin
            newLeftAlignedFrame.origin.y = topMargin
            
            attributes.frame = newLeftAlignedFrame
            
            // calculate new value for current margin
            leftMargin += attributes.frame.size.width
            newAttributesForElementsInRect.append(attributes)
            
        })
        
        collectionSize = CGSize(width: rect.width, height: topMargin + cellHeight)

        
        return newAttributesForElementsInRect
    }
}
