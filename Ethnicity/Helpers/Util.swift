//
//  Util.swift
//  Ethnicity
//
//  Created by  Macbook on 04.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

extension CGSize {
  func insets(by insets: UIEdgeInsets) -> CGSize {
    return CGSize(width: width - insets.left - insets.right, height: height - insets.top - insets.bottom)
  }
    
    func offsets(by insets: UIEdgeInsets) -> CGSize {
        return CGSize(width: width + insets.left + insets.right, height: height + insets.top + insets.bottom)
    }
    
  var transposed: CGSize {
    return CGSize(width: height, height: width)
  }
}
