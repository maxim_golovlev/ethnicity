//
//  NSAttributedString+Ext.swift
//  LentaRecipesStage
//
//  Created by Максим Головлев on 26/08/2019.
//  Copyright © 2019 Калинин Сергей. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func setAlingment(_ alingment: NSTextAlignment) -> NSMutableAttributedString {
        
        let style = NSMutableParagraphStyle()
        style.alignment = alingment
        
        let range = NSRange(location: 0, length: self.length)
        addAttribute(.paragraphStyle, value: style, range: range)
        return self
    }
    
    func setFont(_ font: UIFont?, text: String? = nil) -> NSMutableAttributedString {
        
        var range = NSRange(location: 0, length: self.length)
        
        if let textRange = text != nil ? string.range(of: text!) : nil {
            range = NSRange(textRange, in: self.string)
        }
        
        addAttribute(.font, value: font ?? UIFont.systemFont(ofSize: UIFont.systemFontSize), range: range)
        return self
    }
    
    func setColor(_ color: UIColor?) -> NSMutableAttributedString {
        guard let color = color else { return self }
        let range = NSRange(location: 0, length: self.length)
        addAttribute(.foregroundColor, value: color, range: range)
        return self
    }
    
    func setLink(text: String, url: URL?) -> NSMutableAttributedString {
        guard let range = string.range(of: text), let url = url else { return self }
        addAttributes([.link: url], range: NSRange( range, in: string))
        return self
    }
    
    func setParagraph(style: NSParagraphStyle) -> NSMutableAttributedString {
        let range = NSRange(location: 0, length: self.length)
        addAttribute(.paragraphStyle, value: style, range: range)
        return self
    }
    
    func insertAttachment(image: UIImage) -> NSMutableAttributedString {
        
        let attachment = NSTextAttachment()
        attachment.image = image.withRenderingMode(.alwaysTemplate)
        let side = (attribute(.font, at: 0, effectiveRange: nil) as! UIFont).lineHeight - 2
        attachment.bounds = CGRect(x: 0, y: -2, width: side, height: side)
        
        let attachmentString = NSMutableAttributedString(attachment: attachment)
        attachmentString.append(NSAttributedString(string: " "))
        attachmentString.append(self)
        
        return attachmentString
    }
}
