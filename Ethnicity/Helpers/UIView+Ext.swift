//
//  UIView+Extensions.swift
//  BackMyCashLocal
//
//  Created by mac on 10.07.18.
//  Copyright © 2018 testtesttest. All rights reserved.
//

import Foundation

import UIKit
import Foundation

extension UIView {
    @discardableResult
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat, withShadow: Bool = true, shadowOffset: CGSize? = nil, shadowRadius: CGFloat? = nil, shadowOpacity: Float? = nil, fillColor: CGColor = UIColor.white.cgColor) -> CAShapeLayer {
        
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = path.cgPath
        shadowLayer.fillColor = fillColor
        
        if withShadow {
        
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = shadowOffset ?? CGSize(width: 2.0, height: 1.0)
            shadowLayer.shadowOpacity = shadowOpacity ?? 0.3
            shadowLayer.shadowRadius = shadowRadius ?? 3
            
        }
        
        self.layer.insertSublayer(shadowLayer, at: 0)
        
        return shadowLayer
        
        //   self.layer.mask = mask
    }
    
    func roundCornersMask(_ corners:UIRectCorner, radius: CGFloat) {
        
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = path.cgPath
        self.layer.mask = shadowLayer
    }
    
    func dropShadow() {
        
        let path = UIBezierPath(rect: self.bounds)
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = path.cgPath
        shadowLayer.fillColor = UIColor.white.cgColor
        
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.2
        shadowLayer.shadowRadius = 2
        
        self.layer.insertSublayer(shadowLayer, at: 0)
    }
}

extension UIView {
    
    func dropShadow(_ state: Bool) {
        self.shadowOffset = CGSize(width: 0, height: 2)
        self.shadowOpacity = state ? 0.2 : 0
        self.shadowRadius = state ? 2 : 0
    }
}

@IBDesignable
extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        set {
            layer.shadowOffset = newValue
        }
        get {
            return layer.shadowOffset
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        set {
            layer.shadowOpacity = newValue
        }
        get {
            return layer.shadowOpacity
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        set {
            layer.shadowRadius = newValue
        }
        get {
            return layer.shadowRadius
        }
    }
    
    @IBInspectable var shadowColor: UIColor {
        set {
            layer.shadowColor = newValue.cgColor
        }
        get {
            return UIColor(cgColor: layer.shadowColor!)
        }
    }
    
    @IBInspectable var _clipsToBounds: Bool {
        set {
            clipsToBounds = newValue
        }
        get {
            return clipsToBounds
        }
    }
}

extension UIView {
    
    @discardableResult
    class func fromNib<T : UIView>() -> T? {
        guard let view = Bundle.main.loadNibNamed(String(describing: T.self), owner: self, options: nil)?[0] as? T else {
            return nil
        }
        
        return view
    }
    
    func copyPropertiesFromView(_ view: UIView) {
        self.frame = view.frame
        self.autoresizingMask = view.autoresizingMask
        self.translatesAutoresizingMaskIntoConstraints = view.translatesAutoresizingMaskIntoConstraints
    }
}

extension UIView {
    @IBInspectable var ignoresInvertColors: Bool {
        get {
            if #available(iOS 11.0, *) {
                return accessibilityIgnoresInvertColors
            }
            return false
        }
        set {
            if #available(iOS 11.0, *) {
                accessibilityIgnoresInvertColors = newValue
            }
        }
    }
}

extension UIView {
    
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T{
                all.append(aView)
            }
            guard view.subviews.count>0 else { return }
            view.subviews.forEach{ getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
}

extension UIView {
    func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
}

extension UIViewController {
    
    typealias KeyboardHandler = (Double, UInt, CGFloat, CGFloat)->()
    static var deltaHandler:(KeyboardHandler)?
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
        UIViewController.deltaHandler = nil
    }
    
    func bindToKeyboard(deltaHandler:(KeyboardHandler)? = nil){
        
        UIViewController.deltaHandler = deltaHandler
        
        NotificationCenter.default.removeObserver(self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChange(_:)), name: UIWindow.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardWillChange(_ notification: NSNotification){
        
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let beginningFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let endFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        let deltaY = endFrame.origin.y - beginningFrame.origin.y
        
        let height = max(endFrame.height, beginningFrame.height)
        
        if let handler = UIViewController.deltaHandler {
            handler(duration, curve, deltaY, height)
        }
    }
}

extension UIView {
    
    func incomingMessageLayer(width: CGFloat, height: CGFloat, color: UIColor) -> CAShapeLayer {
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 22, y: height))
        bezierPath.addLine(to: CGPoint(x: width - 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: width, y: height - 17), controlPoint1: CGPoint(x: width - 7.61, y: height), controlPoint2: CGPoint(x: width, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: width, y: 17))
        bezierPath.addCurve(to: CGPoint(x: width - 17, y: 0), controlPoint1: CGPoint(x: width, y: 7.61), controlPoint2: CGPoint(x: width - 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: 4, y: 17), controlPoint1: CGPoint(x: 11.61, y: 0), controlPoint2: CGPoint(x: 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height), controlPoint1: CGPoint(x: 4, y: height - 1), controlPoint2: CGPoint(x: 0, y: height))
        bezierPath.addLine(to: CGPoint(x: -0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: 11.04, y: height - 4.04), controlPoint1: CGPoint(x: 4.07, y: height + 0.43), controlPoint2: CGPoint(x: 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: 22, y: height), controlPoint1: CGPoint(x: 16, y: height), controlPoint2: CGPoint(x: 19, y: height))
        bezierPath.close()
        
        let outgoingMessageLayer = CAShapeLayer()
        outgoingMessageLayer.path = bezierPath.cgPath
        outgoingMessageLayer.frame = bounds
        outgoingMessageLayer.fillColor = color.cgColor
        
        return outgoingMessageLayer
    }
    
    func outgoingMessageLayer(width: CGFloat, height: CGFloat, color: UIColor) -> CAShapeLayer {
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: width - 22, y: height))
        bezierPath.addLine(to: CGPoint(x: 17, y: height))
        bezierPath.addCurve(to: CGPoint(x: 0, y: height - 17), controlPoint1: CGPoint(x: 7.61, y: height), controlPoint2: CGPoint(x: 0, y: height - 7.61))
        bezierPath.addLine(to: CGPoint(x: 0, y: 17))
        bezierPath.addCurve(to: CGPoint(x: 17, y: 0), controlPoint1: CGPoint(x: 0, y: 7.61), controlPoint2: CGPoint(x: 7.61, y: 0))
        bezierPath.addLine(to: CGPoint(x: width - 21, y: 0))
        bezierPath.addCurve(to: CGPoint(x: width - 4, y: 17), controlPoint1: CGPoint(x: width - 11.61, y: 0), controlPoint2: CGPoint(x: width - 4, y: 7.61))
        bezierPath.addLine(to: CGPoint(x: width - 4, y: height - 11))
        bezierPath.addCurve(to: CGPoint(x: width, y: height), controlPoint1: CGPoint(x: width - 4, y: height - 1), controlPoint2: CGPoint(x: width, y: height))
        bezierPath.addLine(to: CGPoint(x: width + 0.05, y: height - 0.01))
        bezierPath.addCurve(to: CGPoint(x: width - 11.04, y: height - 4.04), controlPoint1: CGPoint(x: width - 4.07, y: height + 0.43), controlPoint2: CGPoint(x: width - 8.16, y: height - 1.06))
        bezierPath.addCurve(to: CGPoint(x: width - 22, y: height), controlPoint1: CGPoint(x: width - 16, y: height), controlPoint2: CGPoint(x: width - 19, y: height))
        bezierPath.close()
        
        let outgoingMessageLayer = CAShapeLayer()
        outgoingMessageLayer.path = bezierPath.cgPath
        outgoingMessageLayer.frame = bounds
        outgoingMessageLayer.fillColor = color.cgColor
        
        return outgoingMessageLayer
    }
    
}

extension UIView {
    
    func shake(duration: Double = 0.07, distation: CGFloat = 10) {
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = duration
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - distation, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + distation, y: self.center.y))
        
        self.layer.add(animation, forKey: "position")
        
        
    }
    
}

extension UIView {
    func drawLineFromPoint(start : CGPoint, toPoint end: CGPoint, ofColor lineColor: UIColor) -> CAShapeLayer {
        
        //design the path
        let path = UIBezierPath()
        path.move(to: start)
        path.addLine(to: end)
        
        //design path in layer
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = lineColor.cgColor
        shapeLayer.lineWidth = 1.0
        
        layer.addSublayer(shapeLayer)
        
        return shapeLayer
    }
}

extension UIView {
    
    func animateMove(path: CGPath, duration: TimeInterval) {
        let animation = CAKeyframeAnimation(keyPath: "position")
        animation.path = path
        animation.calculationMode = CAAnimationCalculationMode.paced
        animation.repeatCount = 1
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = .forwards
        
        layer.add(animation, forKey: nil)
    }
    
    func drawCircle(radius: CGFloat, lineWidth: CGFloat = 1, stroke: UIColor, fill: UIColor = .clear, startAngle: CGFloat = 0, endAngle: CGFloat = CGFloat.pi * 2, clockwise: Bool = false, forAnimation: Bool = false, below: CALayer? = nil) -> CAShapeLayer {
        
        let offset = lineWidth / 2 > 1 ? lineWidth / 2 : 0
        let radius1 = radius - offset
        
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        
        let circlePath = UIBezierPath(arcCenter: center, radius: radius1, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = fill.cgColor
        shapeLayer.strokeColor = stroke.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.strokeEnd = forAnimation ? 0 : 1
        
        if let below = below {
            layer.insertSublayer(shapeLayer, below: below)
        } else {
            layer.addSublayer(shapeLayer)
        }
        
        return shapeLayer
    }
}

extension UIView {
    
    //            _ = views.filter({ !($0 is UIControl) }).sorted(by: { (view1, view2) -> Bool in
    //                view1.translatesAutoresizingMaskIntoConstraints = false
    //                view2.translatesAutoresizingMaskIntoConstraints = false
    //                view1.widthAnchor.constraint(equalTo: view2.widthAnchor, multiplier: 1).isActive = true
    //                return true
    //            })

    
}

extension UIView {
    
    public func pauseAnimation(delay: Double) {
        let time = delay + CFAbsoluteTimeGetCurrent()
        let timer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault, time, 0, 0, 0, { timer in
            let layer = self.layer
            let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
            layer.speed = 0.0
            layer.timeOffset = pausedTime
        })
        CFRunLoopAddTimer(CFRunLoopGetCurrent(), timer, CFRunLoopMode.commonModes)
    }
    
    public func resumeAnimation() {
        let pausedTime  = layer.timeOffset
        
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
    }
}

extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}


