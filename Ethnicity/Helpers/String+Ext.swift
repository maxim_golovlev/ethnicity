//
//  String+Ext.swift
//  Ethnicity
//
//  Created by  Macbook on 04.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension String {
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
}

extension Optional where Wrapped == String {
    
    func toDateString(sourceFormat: String, input: String) -> String? {
        
        guard let unwrappedSelf = self else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        guard let date = dateFormatter.date(from: unwrappedSelf) else { return nil }
        dateFormatter.dateFormat = input
        return dateFormatter.string(from: date)
        
    }
//    
//    func toDate(format: String, locale: Locale = Locale(identifier: "ru")) -> Date? {
//        guard let unwrappedSelf = self else { return nil }
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = format
//        dateFormatter.locale = locale
//        return dateFormatter.date(from: unwrappedSelf)
//    }
}

extension String {
    
    func toDate(format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en")
        dateFormatter.timeZone = TimeZone(identifier: "America/Los_Angeles")
        let date = dateFormatter.date(from: self)
        return date
    }
}
