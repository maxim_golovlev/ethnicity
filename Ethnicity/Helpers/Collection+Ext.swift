//
//  Collection+Ext.swift
//  BackMyCash
//
//  Created by User on 12.02.2019.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

extension Collection {
    
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Collection where Element: UIView {

    func setEqualWidth() {
        
        _ = self.filter({ !($0 is UIControl) }).sorted(by: { (view1, view2) -> Bool in
            view1.translatesAutoresizingMaskIntoConstraints = false
            view2.translatesAutoresizingMaskIntoConstraints = false
            view1.widthAnchor.constraint(equalTo: view2.widthAnchor, multiplier: 1).isActive = true
            return true
        })
    }
}

extension Array where Element: Hashable {
    
    @discardableResult
    mutating func appendOptional(element: Element?) -> Array {
        if let element = element {
            append(element)
            return self
        }
        
        return self
    }
    
}

extension Array where Element: URLSessionDataTask {
    
    mutating func cancelAll() {
        forEach({ $0.cancel() })
        removeAll()
    }
}

