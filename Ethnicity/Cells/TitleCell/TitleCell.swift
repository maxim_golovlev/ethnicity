//
//  TitleCell.swift
//  Ethnicity
//
//  Created by  Macbook on 04.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
