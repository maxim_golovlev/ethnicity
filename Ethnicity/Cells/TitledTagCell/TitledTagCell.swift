//
//  TitledTagCell.swift
//  Ethnicity
//
//  Created by  Macbook on 22.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class TitledTagCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.register(UINib(nibName: "TagCell", bundle: nil), forCellWithReuseIdentifier: "TagCell")
            let layout = TagsFlowLayout()
            layout.itemSize = UICollectionViewFlowLayout.automaticSize
            collectionView.collectionViewLayout = layout
        }
    }
    
    var tags = [String]()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        collectionView.reloadData()
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
           
        let size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        
        let collectionWidth = targetSize.width - 100 - 12 * 3
        collectionView.frame = CGRect(x: 0, y: 0, width: collectionWidth, height: 10_000)
        collectionView.layoutIfNeeded()
        
        let height = collectionView.contentSize.height + 4 * 2
        
        return CGSize(width: collectionView.contentSize.width, height: height)
    }
}

extension TitledTagCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
        cell.titleLabel.text = tags[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
}

extension TitledTagCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
       let title = tags[indexPath.item]
        
        let font = UIFont.systemFont(ofSize: 17, weight: .regular)
        let height: CGFloat = font.lineHeight
        let width = title.width(withConstrainedHeight: height, font: font)
        
        return CGSize(width: width + 8 * 2 + 4 * 2, height: height + 8 * 2 + 4 * 2)
        
    }
}
