//
//  CelebFeedCell.swift
//  Ethnicity
//
//  Created by  Macbook on 02.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit


class CelebFeedCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image = nil
    }
    
}
