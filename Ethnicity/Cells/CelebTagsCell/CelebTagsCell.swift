//
//  CelebTagsCell.swift
//  Ethnicity
//
//  Created by  Macbook on 04.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class CelebTagsCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.register(UINib(nibName: "TagCell", bundle: nil), forCellWithReuseIdentifier: "TagCell")
            let layout = TagsFlowLayout()
            layout.itemSize = UICollectionViewFlowLayout.automaticSize
            collectionView.collectionViewLayout = layout
        }
    }
    
    var tags = [String]()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        //collectionView.reloadData()
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
           
        super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        
        collectionView.frame = CGRect(x: 0, y: 0, width: targetSize.width, height: 10_000);
        collectionView.layoutIfNeeded()
        
        let size = collectionView.contentSize.offsets(by: UIEdgeInsets(top: 4, left: 12, bottom: 4, right: 12))
        
        return size
    }
}

extension CelebTagsCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
        cell.titleLabel.text = tags[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
}

extension CelebTagsCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
       let title = tags[indexPath.item]
        
        let font = UIFont.systemFont(ofSize: 17, weight: .regular)
        let height: CGFloat = font.lineHeight
        let width = title.width(withConstrainedHeight: height, font: font) + 8 * 2 + 4 * 2
        
        return CGSize(width: min(width, collectionView.bounds.width) , height: height + 8 * 2 + 4 * 2)
        
    }
}
