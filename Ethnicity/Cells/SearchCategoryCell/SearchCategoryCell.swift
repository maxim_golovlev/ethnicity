//
//  SearchCategoryCell.swift
//  Ethnicity
//
//  Created by Максим Головлев on 26/11/2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class SearchCategoryCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
