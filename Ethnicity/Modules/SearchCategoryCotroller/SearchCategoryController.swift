//
//  SearchCategoryController.swift
//  Ethnicity
//
//  Created by Максим Головлев on 26/11/2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class SearchCategoryController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.tableFooterView = UIView()
            tableView.register(UINib(nibName: "SearchCategoryCell", bundle: nil), forCellReuseIdentifier: "SearchCategoryCell")
        }
    }
    
    var celebs = [NetworthCeleb]()
    
    var categories = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DataManager.fetchNetworthCelebs { [weak self] (celebs) in
            if let celebs = celebs?.sorted(by: { $0.name > $1.name }) {
                self?.celebs = celebs
            }
        }
    }

}

extension SearchCategoryController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = categories[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCategoryCell", for: indexPath) as! SearchCategoryCell
        cell.titleLabel.text = item.name
        return cell
        
    }
}

extension SearchCategoryController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SearchResultController") as! SearchResultController

        let category = categories[indexPath.row]
        
        switch category.type {
        case .age:
            vc.results = self.celebs.filter({
                if let age = $0.celebYears?.age {
                    return category.range?.contains(age) == true
                }
                return false
            }).sorted(by: { $0.celebYears!.age! < $1.celebYears!.age! })
        case .ethnicity:
            if let query = category.query {
                vc.results = self.celebs
                    .filter({ $0.ethnicity?.contains(where: { $0.contains(query) }) == true })
                    .sorted(by: { $0.name! < $1.name! })
            }
        case .gender:
            if let query = category.query, let gender = Gender(rawValue: query) {
                vc.results = self.celebs
                    .filter({ $0.gender == gender })
                    .sorted(by: { $0.name! < $1.name! })
            }
        case .height:
            vc.results = self.celebs.filter({
                if let height = $0.height?.cmValueDouble {
                    return category.range?.contains(Int(height)) == true
                }
                return false
            }).sorted(by: { $0.height!.cmValueDouble! < $1.height!.cmValueDouble! })
        case .nationality:
            if let query = category.query {
                vc.results = self.celebs
                    .filter({ $0.nationality?.contains(query) == true })
                    .sorted(by: { $0.name! < $1.name! })
            }
        case .profession:
            if let query = category.query {
                vc.results = self.celebs.filter({ $0.profession?.contains(where: { $0.contains(query) }) == true }).sorted(by: { $0.name! < $1.name! })
            }
        case .networth:
            vc.results = self.celebs.filter({
                if let netWorth = $0.netWorth?.value {
                    return category.range?.contains(Int(netWorth)) == true
                }
                return false
            }).sorted(by: { $0.netWorth!.value! < $1.netWorth!.value! })
        }
        if presentingViewController != nil {
            presentingViewController?.navigationController?.pushViewController(vc, animated: true)
        } else {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
