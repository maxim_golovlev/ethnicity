//
//  SearchFeedController.swift
//  Ethnicity
//
//  Created by Максим Головлев on 26/11/2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

struct Category {
    var name: String?
    var type: CategoryType
    var range: Range<Int>?
    var query: String?
    
    var subcategories: [Category]?
}

enum CategoryType {
    case age
    case height
    case nationality
    case ethnicity
    case gender
    case profession
    case networth
    
    var name: String {
        switch self {
        case .age:
            return "Age"
        case .height:
            return "Height"
        case .nationality:
            return "Nationality"
        case .ethnicity:
            return "Ethnicity"
        case .gender:
            return "Gender"
        case .profession:
            return "Profession"
        case .networth:
            return "Net Worth"
        }
    }
}

class SearchFeedController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.tableFooterView = UIView()
            
            tableView.register(UINib(nibName: "SearchCategoryCell", bundle: nil), forCellReuseIdentifier: "SearchCategoryCell")
        }
    }
    var eur = ["English", "Swiss", "French", "Polish", "Swedish", "Scottish", "German", "Scots-Irish", "Nothern Irish", "Irish", "French Huguenot", "Dutch", "Swiss-German", "Channel Islander", "Jersey", "Welsh", "Belgian", "Walloon", "Austrian", "Croatian", "Portuguese", "Alsatian", "Cornish", "Spanish", "Indigenous", "Andalusian", "Norwegian", "Neapolitan", "Manx", "Luxembourgish", "Karelian", "Italian", "Ulster-Scots", "Greek", "Sicilian", "Cajun", "Bohemian", "Belgian", "Flemish", "Anglo-Irish", "Alsatian German", "European Royal", "Ukrainian", "Breton", "Rusyn-Slovak", "Azorean", "Venetian", "Galician", "Catalan", "Romani", "Maltese", "Russian", "Slovak", "Hungarian", "Sami", "Latvian", "Baltic German", "Czech", "Basque", "Slovenian", "Volga German", "Canary Islander", "Castilian", "Bosniak", "Serbian", "Turkish", "Albanian", "Belarusian", "Lithuanian", "Danish", "Estonian", "Caymanian", "Finnish", "Rusyn", "Ruthenian", "Icelandic", "Yugoslavian", "Valencian", "Scandinavian", "Corsican", "Bulgarian", "Moravian", "Calabrian", "Barbadian", "Cantabrian", "Frisian", "Aragonese", "Leónese", "Sorbian", "Guernsey", "Extremaduran", "Mordvin", "Asturian", "Abruzzese", "Hispanic", "Cypriot", "Faroese", "Leonese", "Macedonian", "Moldovan"]
    var asia = ["Jewish", "Māori", "Mon", "Filipino", "Chinese", "Bengali", "Indonesian", "Thai", "Syrian", "Malasyan", "Japanese", "Armenian", "Korean", "Indo-Trinidadian", "Indian", "Arab", "Gujarati", "Cebuano", "Iranian", "Vietnamese", "Kalmyk", "Pakistani", "Uzbek", "Tajik", "Libyan", "Moroccan", "Palestinian", "Tatar", "Taiwanese", "Georgian", "Assyrian", "Javanese", "Tagalog", "Waray", "Persian", "Malay", "Burmese", "Tongan", "Afghan", "Azerbaijani", "Bhutanese", "Bicolano", "Boholano", "Cambodian", "Circassian", "Hiligaynon", "Ilocano", "Kapampangan", "Kazakh", "Kurdish", "Pangasinan", "Singaporean", "Sri Lankan"]
    var africa = ["Morrocan Berber", "Angolan", "Ethiopian", "Somali", "Mexican", "Afrikaner", "Malagasy", "Lebanese", "Ndebele", "Haitian", "Moorish", "Kittian", "Nevisian", "Nigerian", "Cameroonian", "Algerian", "Egyptian", "Senegalese", "Guinean", "Cape Verdean", "Jamaican", "Kenyan", "Zimbabwean", "Zambian", "Sudanese", "Sierra Leonean", "Congolese", "Ethiopian", "Nicaraguan", "Ugandan", "Angolan", "Liberian", "Senegal", "Togolese", "Rwandan", "Yoruba", "Ghanaian", "Beninese", "Khoikhoi", "Igbo", "Ivorian", "Cape Coloured", "Fula", "Shona", "South African", "Tanzanian", "Tunisian", "Berber", "Tobagonian"]
    var america = ["Shoshone", "Native American", "Chilean", "French-Canadian", "Cuban", "Cherokee", "Choctaw", "Puerto Rican", "Costa Rican", "Nansemond", "Muisca", "Tahami", "Quechua", "Mi'kmaq", "Tupiniquim", "Osage", "Tamil", "Brazilian", "Lumbee", "Iroquois", "Algonquin", "Blackfoot", "Creole", "Powhatan", "Cree", "Wampanoag", "Venezuelan", "Mandinka", "Honduran", "Colombian", "Peruvian", "Gibraltarian", "Grenadian", "Guatemalan", "Samoan", "Hawaiian", "Sioux", "Dominican", "Virgin Islander", "Tiwa", "Saint Lucian", "Saulteaux", "Bahamian", "Argentinian", "Oneida", "Yup'ik", "Guadeloupean", "Bolivian", "African-American", "Khosian", "Wyandot", "Guyanese", "Trinidadian", "Caribbean", "Panamanian", "Surinamese", "Vincentian", "Taíno", "Lakota", "Martiniquais", "Ecuadorian", "Salvadoran", "Uruguayan", "Curaçaoan"]
    var australia = ["Aboriginal Australian", "Fijian", "Tahitian"]

    lazy var ethnic = eur + asia + africa + america + australia
    
    var categories = [Category]()
    
    lazy var searchViewController: UISearchController = {
        let vc = UISearchController(searchResultsController: searchResultController)
        vc.searchResultsUpdater = searchResultController
        vc.obscuresBackgroundDuringPresentation = false
        vc.searchBar.placeholder = "Search..."
        definesPresentationContext = true
        return vc
    }()
    
    lazy var searchResultController: SearchResultController = {
        let result = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SearchResultController") as! SearchResultController
        return result
    }()
    
    var celebs = [NetworthCeleb]()
    var nationalities = [String]()
    var professions = [String]()
    var ethnicity = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.searchController = searchViewController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        DataManager.fetchNetworthCelebs(completion: { [weak self] (celebs) in
            self?.celebs = celebs ?? []
            
            self?.celebs.forEach({ (celeb) in
                if let nationality = celeb.nationality, self?.nationalities.contains(nationality) == false {
                    self?.nationalities.append(nationality)
                }
                
                if let professions = celeb.profession {
                    professions.forEach({ prof in
                        if self?.professions.contains(prof) == false {
                            self?.professions.append(prof)
                        }
                    })
                }
            })
            
            self?.nationalities = self?.nationalities.sorted(by: { $0 < $1 }) ?? []
            self?.professions = self?.professions.sorted(by: { $0 < $1 }) ?? []
            self?.ethnicity = self?.ethnicity.sorted(by: { $0 < $1 }) ?? []
            
            self?.categories =
            [Category(type: .age,
                     subcategories: [Category(name: "0 - 10 years", type: .age, range: 0..<10),
                                     Category(name: "10 - 30 years", type: .age, range: 10..<30),
                                     Category(name: "31 - 50 years", type: .age, range: 31..<50),
                                     Category(name: "51 - 70 years", type: .age, range: 51..<70),
                                     Category(name: "71 - 90 years", type: .age, range: 71..<90),
                                     Category(name: "91 years and more", type: .age, range: 91..<100),
                                     Category(name: "100 years and more", type: .age, range: 100..<190)]),
            Category(type: .height,
                     subcategories: [Category(name: "0 - 120 cm", type: .height, range: 0..<120),
                                     Category(name: "121 - 160 cm", type: .height, range: 121..<160),
                                     Category(name: "161 - 170 cm", type: .height, range: 161..<170),
                                     Category(name: "171 - 175 cm", type: .height, range: 171..<175),
                                     Category(name: "176 - 180 cm", type: .height, range: 176..<180),
                                     Category(name: "181 - 185 cm", type: .height, range: 181..<185),
                                     Category(name: "186 - 195 cm", type: .height, range: 186..<195),
                                     Category(name: "196 cm and more", type: .height, range: 196..<300),
                                     ]),
            Category(type: .nationality, subcategories: self?.nationalities.compactMap({ Category(name: $0, type: .nationality, query: $0) })),
            Category(type: .ethnicity, subcategories: self?.ethnic.compactMap({ Category(name: $0, type: .ethnicity, query: $0) })),
            Category(type: .gender,
                     subcategories: [Category(name: "Male", type: .gender, query: "Male"),
                                    Category(name: "Female", type: .gender, query: "Female")]),
            Category(type: .profession, subcategories: self?.professions.compactMap({ Category(name: $0, type: .profession, query: $0) })),
            Category(type: .networth,
                     subcategories: [Category(name: "0 - 1 million", type: .networth, range: 0..<1000_000),
                                     Category(name: "1 - 10 million", type: .networth, range: 1000_000..<10_000_000),
                                     Category(name: "10 - 100 million", type: .networth, range: 10_000_000..<100_000_000),
                                     Category(name: "0.1 - 1 billion", type: .networth, range: 100_000_000..<1000_000_000),
                                     Category(name: "1 - 10 billion", type: .networth, range: 1000_000_000..<10_000_000_000),
                                     Category(name: "10 - 100 billion", type: .networth, range: 10_000_000_000..<100_000_000_000),
                                     Category(name: "100 billions and more", type: .networth, range: 100_000_000_000..<1000_000_000_000_000)
              ].reversed())
            ]

        })
        
        
    }
}

extension SearchFeedController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let category = categories[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCategoryCell", for: indexPath) as! SearchCategoryCell
        cell.titleLabel.text = category.type.name
        return cell
    }
}


extension SearchFeedController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SearchCategoryController") as! SearchCategoryController
        vc.categories = categories[indexPath.row].subcategories ?? []
        navigationController?.pushViewController(vc, animated: true)
        
    }
}
