//
//  SearchResultController.swift
//  Ethnicity
//
//  Created by Максим Головлев on 26/11/2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class SearchResultController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.tableFooterView = UIView()
            tableView.register(UINib(nibName: "SearchResultCell", bundle: nil), forCellReuseIdentifier: "SearchResultCell")
        }
    }
    
    var celebs = [NetworthCeleb]()
    
    var results = [NetworthCeleb]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DataManager.fetchNetworthCelebs { [weak self] (celebs) in
            if let celebs = celebs?.sorted(by: { $0.name > $1.name }) {
                self?.celebs = celebs
            }
        }
        
    }

}

extension SearchResultController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = results[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath) as! SearchResultCell
        cell.avatarView.setImage(urlString: item.picture)
        cell.titleLabel.text = item.name
        return cell
        
    }
}

extension SearchResultController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CelebrityController") as! CelebrityController

        vc.celebrityNetworth = results[indexPath.item]
        
        if presentingViewController != nil {
            presentingViewController?.navigationController?.pushViewController(vc, animated: true)
        } else {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension SearchResultController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        results = []
        
        if let query = searchController.searchBar.text, !query.isEmpty {
            results = celebs.filter({ $0.name.lowercased().contains(query.lowercased()) })
        }
        
        tableView.reloadData()
    }
    
}
