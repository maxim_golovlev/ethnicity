//
//  FeedViewController.swift
//  Ethnicity
//
//  Created by  Macbook on 02.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    var newPicCeleb = [NetworthCeleb]()
    
    var oldCelebs = [Celebrity]()
    var celebs = [NetworthCeleb]()
    
    let group = DispatchGroup()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataManager.fetchNetworthCelebs { (celeb) in
            
            self.celebs = celeb?.filter({ $0.ethnicity != nil }).shuffled() ?? []
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
}

extension FeedViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return celebs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let celeb = celebs[indexPath.item]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CelebFeedCell", for: indexPath) as! CelebFeedCell
        cell.imageView.setImage(urlString: celeb.picture)
        return cell
    }
    
}

extension FeedViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let bounds = UIScreen.main.bounds
        
        let width = bounds.width / 2
        let height = bounds.height / 3
        
        return CGSize(width: width, height: height)
    }
    
}

extension FeedViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CelebrityController") as! CelebrityController

        vc.celebrityNetworth = celebs[indexPath.item]
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
