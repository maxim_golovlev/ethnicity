//
//  CelebrityController.swift
//  Ethnicity
//
//  Created by  Macbook on 04.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class CelebrityController: UIViewController {

    enum RawType {
        case title
        case picture
        case plane
        case description
        case tags
        case titledTags
    }
    
    struct Row {
        var title: String?
        var body = [String]()
        var type: RawType
    }
    
    struct Section {
        var title: String?
        var rows = [Row]()
        var type: RawType
    }
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            tableView.register(UINib(nibName: "CelebInfoCell", bundle: nil), forCellReuseIdentifier: "CelebInfoCell")
            tableView.register(UINib(nibName: "CelebDesriptionCell", bundle: nil), forCellReuseIdentifier: "CelebDesriptionCell")
            tableView.register(UINib(nibName: "CelebTagsCell", bundle: nil), forCellReuseIdentifier: "CelebTagsCell")
            
            tableView.register(UINib(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: "ImageCell")
            tableView.register(UINib(nibName: "TitleCell", bundle: nil), forCellReuseIdentifier: "TitleCell")
            tableView.register(UINib(nibName: "TitledTagCell", bundle: nil), forCellReuseIdentifier: "TitledTagCell")
        }
    }
    
    var sections = [Section]()
    
    var celebrityNetworth: NetworthCeleb? {
        didSet {
            
            if let name = celebrityNetworth?.name {
                var nameSection = Section(type: .title)
                nameSection.rows.append(Row(body: [name], type: .title))
                sections.append(nameSection)
            }
            
            if let image = celebrityNetworth?.picture {
                var avatarSection = Section(type: .picture)
                avatarSection.rows.append(Row(body: [image], type: .picture))
                sections.append(avatarSection)
            }
            
            var simpleSection = Section(type: .plane)
            
            if let birthName = celebrityNetworth?.birthName {
                simpleSection.rows.append(Row(title: "Birth Name", body: [birthName], type: .plane))
            }
            
            if let celebYears = celebrityNetworth?.celebYears?.string {
                simpleSection.rows.append(Row(title: "Date of Birth", body: [celebYears], type: .plane))
            }
            
            if let birthPlace = celebrityNetworth?.birthPlace {
                simpleSection.rows.append(Row(title: "Birth Place", body: [birthPlace], type: .plane))
            }
            
            if let gender = celebrityNetworth?.gender, !gender.rawValue.isEmpty {
                simpleSection.rows.append(Row(title: "Gender", body: [gender.rawValue], type: .titledTags))
            }
            
            if let height = celebrityNetworth?.height?.fullstring {
                simpleSection.rows.append(Row(title: "Height", body: [height], type: .titledTags))
            }
            
            if let ethnicity = celebrityNetworth?.ethnicity?.joined(separator: ", ") {
                simpleSection.rows.append(Row(title: "Ethnicity", body: [ethnicity], type: .plane))
            }
            
            if let nationality = celebrityNetworth?.nationality, !nationality.isEmpty {
                simpleSection.rows.append(Row(title: "Nationality", body: [nationality], type: .titledTags))
            }
            
            if let profession = celebrityNetworth?.profession?.prefix(5).sorted(by: { $0.count < $1.count }), !profession.isEmpty {
                simpleSection.rows.append(Row(title: "Profession", body: profession, type: .titledTags))
            }
            
            if let netWorth = celebrityNetworth?.netWorth?.string, !netWorth.isEmpty {
                simpleSection.rows.append(Row(title: "Net Worth", body: [netWorth], type: .titledTags))
            }
            
            sections.append(simpleSection)
            
            if let description = celebrityNetworth?.description, !description.isEmpty {
                var descriptionSection = Section(title: "Biography", type: .description)
                descriptionSection.rows.append(Row(body: [description], type: .description))
                sections.append(descriptionSection)
            }

            if let similar = celebrityNetworth?.similar, !similar.isEmpty {
                var similarSection = Section(title: "Similar", type: .tags)
                similarSection.rows.append(Row(body: similar, type: .tags))
                sections.append(similarSection)
            }

            if let similar = celebrityNetworth?.similar2, !similar.isEmpty {
                var similarSection = Section(title: "Similar2", type: .tags)
                similarSection.rows.append(Row(body: similar, type: .tags))
                sections.append(similarSection)
            }

            if let tags = celebrityNetworth?.tags, !tags.isEmpty {
                var tagsSection = Section(title: "Tags", type: .tags)
                tagsSection.rows.append(Row(body: tags, type: .tags))
                sections.append(tagsSection)
            }

            if let categories = celebrityNetworth?.categories, !categories.isEmpty {
                var categoriesSection = Section(title: "Categories", type: .tags)
                categoriesSection.rows.append(Row(body: categories, type: .tags))
                sections.append(categoriesSection)
            }
        }
    }
    
    var celebrity: Celebrity? {
        didSet {
            
            if let name = celebrity?.name {
                var nameSection = Section(type: .plane)
                nameSection.rows.append(Row(body: [name], type: .plane))
                sections.append(nameSection)
            }
            
            if let image = celebrity?.picture {
                var avatarSection = Section(type: .plane)
                avatarSection.rows.append(Row(body: [image], type: .plane))
                sections.append(avatarSection)
            }
            
            var simpleSection = Section(type: .plane)
            
            if let birthName = celebrity?.birthName {
                simpleSection.rows.append(Row(title: "Birth Name", body: [birthName], type: .plane))
            }
            
            if let height = celebrity?.height?.height {
                simpleSection.rows.append(Row(title: "Height", body: [height], type: .plane))
            }
            
            if let birthPlace = celebrity?.birthPlace {
                simpleSection.rows.append(Row(title: "Birth Place", body: [birthPlace], type: .plane))
            }
            
            if let dateOfBirth = celebrity?.dateOfBirth {
                simpleSection.rows.append(Row(title: "Date of Birth", body: [dateOfBirth], type: .plane))
            }
            
            if let deathPlace = celebrity?.deathPlace, !deathPlace.isEmpty {
                simpleSection.rows.append(Row(title: "Death Place", body: [deathPlace], type: .plane))
            }
            
            if let dateOfDeath = celebrity?.dateOfDeath, !dateOfDeath.isEmpty {
                simpleSection.rows.append(Row(title: "Date of Death", body: [dateOfDeath], type: .plane))
            }
            
            if let ethnicity = celebrity?.ethnicity, !ethnicity.isEmpty {
                simpleSection.rows.append(Row(title: "Ethnicity", body: ethnicity, type: .plane))
            }
            
            sections.append(simpleSection)
            
            if let description = celebrity?.description, !description.isEmpty {
                var descriptionSection = Section(title: "Biography", type: .description)
                descriptionSection.rows.append(Row(body: [description], type: .description))
                sections.append(descriptionSection)
            }
            
            if let similar = celebrity?.similar, !similar.isEmpty {
                var similarSection = Section(title: "Similar", type: .tags)
                similarSection.rows.append(Row(body: similar, type: .tags))
                sections.append(similarSection)
            }
            
            
            if let tags = celebrity?.tags, !tags.isEmpty {
                var tagsSection = Section(title: "Tags", type: .tags)
                tagsSection.rows.append(Row(body: tags, type: .tags))
                sections.append(tagsSection)
            }
            
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

}

extension CelebrityController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = sections[indexPath.section]
        let row = section.rows[indexPath.item]
        let type = row.type
        
        switch type {
        case .title:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.label.text = row.body.first
            return cell
        case .picture:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
            cell.pictureView.setImage(urlString: row.body.first)
            return cell
        case .plane:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CelebInfoCell", for: indexPath) as! CelebInfoCell
            cell.title.text = row.title
            cell.body.text = row.body.first
            return cell
        case .description:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CelebDesriptionCell", for: indexPath) as! CelebDesriptionCell
            cell.bodyLabel.text = row.body.first?.replacingOccurrences(of: "\n\n", with: " ")
            return cell
        case .tags:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CelebTagsCell", for: indexPath) as! CelebTagsCell
            cell.tags = row.body
            return cell
        case .titledTags:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitledTagCell", for: indexPath) as! TitledTagCell
            cell.label.text = row.title
            cell.tags = row.body
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let section = sections[section]
        let type = section.type
        
        switch type {
        case .title:
            return nil
        case .plane:
            return nil
        case .description:
            return section.title
        case .tags:
            return section.title
        case .picture:
            return nil
        case .titledTags:
            return nil
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 || section == 2 {
            return 10
        }
        return 20
    }
}

extension CelebrityController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section = sections[indexPath.section]
        let row = section.rows[indexPath.row]
        let type = row.type
        
        switch type {
        case .title:
            break
        case .plane:
            break
        case .description:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "DescriptionViewController") as! DescriptionViewController
            vc.bodyText = row.body.first
            navigationController?.pushViewController(vc, animated: true)
        case .tags:
            break
        case .picture:
            break
        case .titledTags:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        let section = sections[indexPath.section]
        let row = section.rows[indexPath.row]
        let type = row.type
        
        switch type {
        case .title:
            return UITableView.automaticDimension //UIFont.systemFont(ofSize: 22, weight: .bold).lineHeight + 12 * 2
        case .plane:
            return UITableView.automaticDimension
        case .description:
            return UITableView.automaticDimension
        case .tags:
            return UITableView.automaticDimension
        case .picture:
            return 350
        case .titledTags:
            return UITableView.automaticDimension
        }
        
    }
}
