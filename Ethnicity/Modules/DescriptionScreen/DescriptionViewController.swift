//
//  DescriptionViewController.swift
//  Ethnicity
//
//  Created by  Macbook on 05.11.2019.
//  Copyright © 2019 Golovelv Maxim. All rights reserved.
//

import UIKit

class DescriptionViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    var bodyText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineSpacing = 7

        if let bodyText  = bodyText {
            self.textView.attributedText = NSMutableAttributedString(string: bodyText).setFont(textView.font).setParagraph(style: paragraph)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
